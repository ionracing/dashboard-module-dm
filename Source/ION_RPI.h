#include "stm32f4xx.h"

void RPI_Initialise(USART_TypeDef * USARTx);

typedef struct
{
    uint16_t id;
    uint32_t data;
} rpiMsg;
