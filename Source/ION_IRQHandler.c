#include "stm32f4xx_usart.h"
#include "ION_USART.h"
#include "ION_GPIO.h"

uint8_t RxByte = 0;
void USART1_IRQHandler(void){
    GPIO_SetBits(GPIOD, GPIO_Pin_14);
	// check if the USART1 receive interrupt flag was set
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART1,USART_IT_RXNE);
        USART_Read(USART1);
	}
}
void USART2_IRQHandler(void){
    GPIO_SetBits(GPIOD, GPIO_Pin_14);
	// check if the USART2 receive interrupt flag was set
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		USART_ClearITPendingBit(USART2,USART_IT_RXNE);
		USART_Read(USART2);
	}
}
