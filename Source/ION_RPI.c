#include "ION_RPI.h"
#include "ION_USART.h"

/**
 * We are useing a Raspberry Pi 2 model B and are communicating to this with USART.
 */
 
 USART_TypeDef * USART = USART1;

void RPI_Initialise(USART_TypeDef * USARTx)
{
    USART = USARTx;
    USART_Initialise(USARTx, 9600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Tx | USART_Mode_Rx, USART_HardwareFlowControl_None);
}

uint8_t s = 0;
uint32_t* point;
void RPI_Write(uint32_t* Data)
{
    
    for(point = Data;point < point+32;point += 8)
    {
        
        USART_Write(USART, *point);
    }
}
