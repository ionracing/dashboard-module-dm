#include "ION_USART.h"
#include "ION_GPS.h"
#include "stm32f4xx_usart.h"
#include "stdio.h"

/**
 * We are using Adafruit Ultimate GPS Breakout that you can find here.
 * https://www.adafruit.com/products/746
 *
 * We are using USART2 and are connected like this.
 * Gps	-	stm32f4xx
 * 3.3V	- 	Vdd
 * Gnd 	- 	Gnd
 * RX	-	PA2
 * TX 	- 	PA3
 *
 * We use 3.3V instead of 5V.
 *
 */

char end[] = "\r\n";
int g = 0;
uint8_t RxByte1 = 0;
char *t;
void GPS_Initialise(USART_TypeDef* USARTx)
{
	USART_Initialise(USARTx, 9600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Tx | USART_Mode_Rx, USART_HardwareFlowControl_None);
    
    sendString(USARTx, PMTK_SET_NMEA_OUTPUT_RMCONLY);
    if(x == 1)
    {
        //success
    }
    else
    {
        //try agian
        if(x == 0)
        {
            //send error on usart.
            //break out. Do not turn on gps.
        }
    }
    sendString(USARTx, PMTK_SET_NMEA_UPDATE_10HZ);
    for(g = 0;g < 0xFFFFF;g++);
    sendString(USARTx, PMTK_API_SET_FIX_CTL_5HZ);
}
void sendString(USART_TypeDef* USARTx, char *chr)//changed this from char chr[]
{
    while(*chr != '\0')
    {
        USART_Write(USARTx, *chr);
        chr++;
    }
    t = &end[0];
    while(*t != '\0')
    {
        USART_Write(USARTx, *t);
        t++;
    }
}
