#include "ION_CAN.h"
#include "ION_GPIO.h"
#include "stm32f4xx_gpio.h"
#include "misc.h"

void CAN_Initialise(CAN_TypeDef * CANx)
{
	CAN_InitTypeDef CAN_InitStruct;
    CAN_FilterInitTypeDef  CAN_FilterInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
	
	CAN_GPIO_Initialise(CANx);
	
	CAN_RCC_Initialise(CANx);
	
	/* CAN register init */
	CAN_DeInit(CANx);
	
	/* CAN cell init */
	CAN_InitStruct.CAN_TTCM = DISABLE;
	CAN_InitStruct.CAN_ABOM = DISABLE;
	CAN_InitStruct.CAN_AWUM = DISABLE;
	CAN_InitStruct.CAN_NART = DISABLE;
	CAN_InitStruct.CAN_RFLM = DISABLE;
	CAN_InitStruct.CAN_TXFP = DISABLE;
	CAN_InitStruct.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStruct.CAN_SJW = CAN_SJW_3tq;

    //Bus length 3-5m.
    
    
	/* CAN Baudrate = 500 KBps (CAN clocked at 42 MHz) */
	CAN_InitStruct.CAN_BS1 = CAN_BS1_4tq;
	CAN_InitStruct.CAN_BS2 = CAN_BS2_2tq;
	CAN_InitStruct.CAN_Prescaler = (42000000 / 7) / 500000; // -> 12
	CAN_Init(CANx, &CAN_InitStruct);

	/* CAN filter init */
	CAN_FilterInitStructure.CAN_FilterNumber = CANx == CAN1 ? 0 : 14;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CANx, CAN_IT_FMP0, ENABLE);

	/* Enable CAN1 RX0 interrupt IRQ channel */
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

void CAN_GPIO_Initialise(CAN_TypeDef * CANx)
{
	if(CANx == CAN1)
	{
		GPIO_Initialise(GPIOB,GPIO_Mode_AF,GPIO_OType_PP,GPIO_Pin_8 | GPIO_Pin_9,GPIO_PuPd_UP,GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1); //RX
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1); //TX
	}
	else if(CANx == CAN2)
	{
		GPIO_Initialise(GPIOB,GPIO_Mode_AF,GPIO_OType_PP,GPIO_Pin_5 | GPIO_Pin_6,GPIO_PuPd_UP,GPIO_Speed_50MHz);
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_CAN2); //RX
		GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_CAN2); //TX
	}
}

/**
 *
 *
 */
void CAN_RCC_Initialise(CAN_TypeDef * CANx)
{
    if (CANx == CAN1)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1,ENABLE);
    }
    else if (CANx == CAN2)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1,ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2,ENABLE);
    }
    else
    {
      while(1);
    }
}

// CAN Transmit
uint8_t i = 0;

uint8_t CAN_Write(CAN_TypeDef * CANx, uint32_t address, uint8_t length, uint8_t data[8])
{
    CanTxMsg msg;
	msg.StdId 	= address;
	msg.IDE 	= CAN_Id_Standard;
	msg.RTR		= CAN_RTR_Data;
	msg.DLC		= length;
    
	for(i=0; i < length; i++)
    {
		msg.Data[i] = data[i];
	}
	return CAN_Transmit(CANx, &msg);
}

// CAN Transmit 16bit.
uint8_t j = 0;
uint8_t data_LSB[8];
uint8_t data_MSB[8];

void CAN_Write_16(CAN_TypeDef * CANx, uint32_t address, uint8_t length, uint16_t data[8])
{
    for(j = 0;j < length;j++)
    {
        data_LSB[j] = (uint8_t)(data[j]);
        data_MSB[j] = (uint8_t)(data[j] >> 8);
    }
    CAN_Write(CANx, address, length, data_LSB);
    CAN_Write(CANx, address, length, data_MSB);
}

/*
CAN RX Interrupt
*/

CanRxMsg msgRx;

void CAN2_RX0_IRQHandler (void){
	__disable_irq();
	if(CAN2->RF0R & CAN_RF0R_FMP0)
	{
		CAN_Receive(CAN2, CAN_FIFO0, &msgRx);
        GPIO_SetBits(GPIOD, GPIO_Pin_13);
        //Add msgRx to a buffer and prosess it another place.
        if(*msgRx.Data == 0xFF)
        {
            GPIO_SetBits(GPIOD, GPIO_Pin_14);
        }
	}
	__enable_irq();
}

void CAN1_RX0_IRQHandler (void){
	__disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0)
	{
		CAN_Receive(CAN1, CAN_FIFO0, &msgRx);
        GPIO_SetBits(GPIOD, GPIO_Pin_13);
        //Add msgRx to a buffer and prosess it another place.
	}
	__enable_irq();
}
